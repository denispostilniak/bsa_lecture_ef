﻿using System;
using System.Collections.Generic;
using System.Text;
using ProjectStructure.Common.Models.DTO;

namespace ProjectStructure.BL.Interfaces
{
    public interface IProjectService
    {
        IEnumerable<ProjectDTO> GetAllProjects();

        void CreateProject(ProjectDTO project);
        void UpdateProject(ProjectDTO project);
        void DeleteProject(int projectId);
    }
}
