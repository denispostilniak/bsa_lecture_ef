﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ProjectStructure.BL.UnitOfWork.Interfaces
{
    public interface IRepository<T> where T: class
    {
        IEnumerable<T> Get();
        void Create(T entity);
        void Update(T entity);
        void Delete(object id);
        void Delete(T entity);
    }
}
