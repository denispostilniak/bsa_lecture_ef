﻿using System;
using System.Collections.Generic;
using System.Text;
using AutoMapper;
using ProjectStructure.BL.Interfaces;
using ProjectStructure.BL.UnitOfWork.Interfaces;
using ProjectStructure.Common.Models.DTO;
using ProjectStructure.DAL.Context;
using ProjectStructure.DAL.Models;

namespace ProjectStructure.BL.Services
{
    public class UserService : IUserService
    {
        private readonly IMapper _mapper;
        private readonly IRepository<User> _repository;
        private readonly IUnitOfWork _unitOfWork;
        public UserService(IUnitOfWork unitOfWork, IMapper mapper) {
            _mapper = mapper;
            _unitOfWork = unitOfWork;
            _repository = _unitOfWork.Set<User>();
        }
        public void CreateUser(UserDTO userDTO) {
            var user = _mapper.Map<User>(userDTO);

            _repository.Create(user);
            _unitOfWork.SaveChanges();
        }

        public void DeleteUser(int userId) {
            _repository.Delete(userId);
            _unitOfWork.SaveChanges();
        }

        public IEnumerable<UserDTO> GetAllUsers() {
            var result = _repository.Get();
            return _mapper.Map<IEnumerable<UserDTO>>(result);
        }

        public void UpdateUser(UserDTO userDTO) {
            var user = _mapper.Map<User>(userDTO);

            _repository.Update(user);
            _unitOfWork.SaveChanges();
        }
    }
}
