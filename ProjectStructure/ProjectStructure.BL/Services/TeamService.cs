﻿using System;
using System.Collections.Generic;
using System.Text;
using AutoMapper;
using ProjectStructure.BL.Interfaces;
using ProjectStructure.BL.UnitOfWork.Interfaces;
using ProjectStructure.Common.Models.DTO;
using ProjectStructure.DAL.Context;
using ProjectStructure.DAL.Models;

namespace ProjectStructure.BL.Services
{
    public class TeamService : ITeamService
    {
        private readonly IMapper _mapper;
        private readonly IRepository<Team> _repository;
        private readonly IUnitOfWork _unitOfWork;
        public TeamService(IUnitOfWork unitOfWork, IMapper mapper) {
            _mapper = mapper;
            _unitOfWork = unitOfWork;
            _repository = _unitOfWork.Set<Team>();
        }
        public void CreateTeam(TeamDTO teamDTO) {
            var team = _mapper.Map<Team>(teamDTO);

            _repository.Create(team);
            _unitOfWork.SaveChanges();
        }

        public void DeleteTeam(int teamId) {
            _repository.Delete(teamId);
            _unitOfWork.SaveChanges();
        }

        public IEnumerable<TeamDTO> GetAllTeams() {
            var result = _repository.Get();
            return _mapper.Map<IEnumerable<TeamDTO>>(result);
        }

        public void UpdateTeam(TeamDTO teamDTO) {
            var team = _mapper.Map<Team>(teamDTO);

            _repository.Update(team);
            _unitOfWork.SaveChanges();
        }
    }
}
